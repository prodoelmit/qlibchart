#include "chartsettings.h"
#include <QSettings>


ChartSettings::ChartSettings(Chart *chart)
	: m_chart(chart)
{

}

void ChartSettings::save(const QString &filename)
{
	QValueAxis* xAxis = dynamic_cast<QValueAxis*>(m_chart->axisX());
	QValueAxis* yAxis = dynamic_cast<QValueAxis*>(m_chart->axisY());
	QSettings s(filename, QSettings::IniFormat);
	s.clear();

	//canvas
	s.beginGroup("canvas");
	s.setValue("size", m_chart->size());
	s.setValue("legendAlignment", (int)(m_chart->legend()->alignment()));
	s.setValue("axesFontPointSize", m_chart->axisX()->labelsFont().pointSizeF());
	s.setValue("legendFontPointSize", m_chart->legend()->font().pointSizeF());
	s.setValue("backgroundBrush", m_chart->backgroundBrush());
	s.endGroup();

    s.beginGroup("xAxis"); s.setValue("columnId", m_chart->m_xCol);
	if (m_chart->m_hasHeaders) {
		s.setValue("columnName", m_chart->m_headers.at(m_chart->m_xCol));
	}
	s.setValue("majorTickCount", xAxis->tickCount());
	s.setValue("minorTickCount", xAxis->minorTickCount());
	s.setValue("label", xAxis->titleText());
	s.setValue("gridVisible", xAxis->isGridLineVisible());
	s.setValue("minorGridVisible", xAxis->isMinorGridLineVisible());
	s.setValue("gridPen", xAxis->gridLinePen());
    s.endGroup();


	s.beginGroup("yAxis");
	s.setValue("majorTickCount", yAxis->tickCount());
	s.setValue("minorTickCount", yAxis->minorTickCount());
	s.setValue("label", yAxis->titleText());
	s.setValue("gridVisible", yAxis->isGridLineVisible());
	s.setValue("gridPen", yAxis->gridLinePen());
	s.endGroup();

	QList<int>& yCols = m_chart->m_yCols;
	s.beginWriteArray("series", yCols.size());
	for (int i = 0; i < yCols.size(); i++) {
		s.setArrayIndex(i);
		QSplineSeries* series = dynamic_cast<QSplineSeries*>(m_chart->series().at(i));
		s.setValue("columnId", yCols.at(i));
		if (m_chart->m_hasHeaders) {
			s.setValue("columnName", m_chart->m_headers.at(yCols.at(i)));
		}
		s.setValue("pen", series->pen());
		s.setValue("brush", series->brush());
		s.setValue("name", series->name());
	} s.endArray();

}

void ChartSettings::load(const QString &filename)
{
	QValueAxis* xAxis = dynamic_cast<QValueAxis*>(m_chart->axisX());
	QValueAxis* yAxis = dynamic_cast<QValueAxis*>(m_chart->axisY());
	QSettings s(filename, QSettings::IniFormat);

	//<canvas>

	s.beginGroup("canvas");
	QSizeF size = s.value("size", QSizeF(800,600)).toSizeF();
	m_chart->resize(size);

	QBrush brush = s.value("backgroundBrush", QBrush(Qt::white, Qt::SolidPattern)).value<QBrush>();
	m_chart->setBackgroundBrush(brush);

	Qt::Alignment legendAlignment = (Qt::Alignment)(s.value("legendAlignment", QVariant((int)(Qt::AlignLeft))).toInt());
	m_chart->legend()->setAlignment(legendAlignment);

	qreal axesFontPointSize = s.value("axesFontPointSize", 12).toDouble();
	auto axesFont = m_chart->axisX()->labelsFont();
	axesFont.setPointSizeF(axesFontPointSize);
	m_chart->axisX()->setLabelsFont(axesFont);
	m_chart->axisY()->setLabelsFont(axesFont);

	qreal legendFontPointSize = s.value("legendFontPointSize", 12).toDouble();
	qDebug() << legendFontPointSize;
	auto legendFont = m_chart->legend()->font();
	legendFont.setPointSizeF(legendFontPointSize);
	m_chart->legend()->setFont(legendFont);

	s.endGroup();

	//</canvas>


	//<xAxis>
	s.beginGroup("xAxis");

	int xCol = s.value("columnId", 0).toInt();
	m_chart->setXColumn(xCol);

	QString label = s.value("label", "").toString();
	qDebug() << label;
	qDebug() << m_chart->axisX();
	m_chart->axisX()->setTitleVisible(true);
	dynamic_cast<QValueAxis*>(m_chart->axisX())->setTitleText(label);

	int xMajorTickCount = s.value("majorTickCount", 0).toInt();
	int xMinorTickCount = s.value("MinorTickCount", 0).toInt();
	bool gridVisible = s.value("gridVisible", true).toBool();
	bool minorGridVisible = s.value("minorGridVisible", false).toBool();

	xAxis->setTickCount(xMajorTickCount);
	xAxis->setMinorTickCount(xMinorTickCount);
	xAxis->setGridLineVisible(gridVisible);
	xAxis->setMinorGridLineVisible(minorGridVisible);


	s.endGroup();
	//</xAxis>

    // <series>

    QList<int> yCols;
    int yColsSize = s.beginReadArray("series");
    for (int i = 0; i < yColsSize; i++) {
        s.setArrayIndex(i);
        int colId = s.value("columnId", 0).toInt();
        yCols << colId;

    } s.endArray();

    m_chart->setYCols(yCols);

    m_chart->prepareSeries();

    // </series>

}
