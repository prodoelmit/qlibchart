#ifndef CSVVALUE_H
#define CSVVALUE_H
#include <QString>

/**
 * @class CSVValue
 * @brief Convenience class for handling CSV values, which can be string or number
 */
class CSVValue
{
public:
	CSVValue(QString s);

    bool isNumber() const;
    bool isString() const;

    qreal number() const;

private:
    bool m_isNumber;
	QString m_string;
	qreal m_number;
};

#endif // CSVVALUE_H
