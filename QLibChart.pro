#-------------------------------------------------
#
# Project created by QtCreator 2016-08-16T15:42:50
#
#-------------------------------------------------

QT       += core gui charts  svg

TARGET = QLibChart
TEMPLATE = lib

DEFINES += QLIBCHART_LIBRARY

SOURCES += qlibchart.cpp \
    chart.cpp \
    csvvalue.cpp \
    chartsettings.cpp

HEADERS += qlibchart.h\
        qlibchart_global.h \
    chart.h \
    csvvalue.h \
    chartsettings.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES += \
    requirements.txt
