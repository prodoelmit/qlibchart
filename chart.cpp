#include "chart.h"
#include <QSvgGenerator>
#include <QPainter>
#include <QImage>
/*!
 * \class Chart
 * \inmodule QLibChart
 * \brief Class which acts as a container for all chart-related information, including it's scene
 */



/*!
   \property Chart::hasHeaders
   \brief Whether first line of CSV input file will be interpreted as header line
  */

/*!
  \property Chart::xNiceNumbers
  \brief Whether x-axis should have only nice numbers on ticks
  */


/*!
  \brief Initializes chart, creating a scene in which it's added. Sets \l QChart::backgroundVisible to \c true, \l QChart::plotAreaBackgroundVisible
  to \c true, \l QChart::backgroundBrush to  QBrush(Qt::white, Qt::SolidPattern)
 */
Chart::Chart()
	: QChart()
	, m_hasHeaders(false)
	, m_xNiceNumbers(false)
{

	QGraphicsScene* scene = new QGraphicsScene;
	scene->addItem(this);
//	QFont font = titleFont();
//	font.setPointSize(14);
//	setTitleFont(font);
//	legend()->setFont(font);
	setBackgroundVisible(true);
	setPlotAreaBackgroundVisible(true);
	QBrush brush(Qt::white, Qt::SolidPattern);
	setBackgroundBrush(brush);


}


/*!
  \brief reads CSV file specified by \a filename. If \a hasHeaders is \c true, interprets first line as headers, else
  interprets it as one more data row. Columns get splitted by separator \a sep
 */
void Chart::readCSV(QString filename, bool hasHeaders, QString sep)
{
	m_hasHeaders = hasHeaders;
	QFile f(filename);
	if (!f.open(QFile::ReadOnly | QFile::Text)) {
		qDebug() << "Could not open CSV for reading: \nfilename:"
				 << filename << "\nreason: " << f.errorString();
		return;
	}
	m_data.clear();

	QTextStream in(&f);

	QString line;
	int i = 0;
	while (!in.atEnd()) {
		in.readLineInto(&line);
		QStringList list = line.split(sep);
		if (!i) {
			m_headers = list;
		} else {
			QList<qreal> values;
			foreach (QString s, list) {
				values << s.toDouble();
			}
			m_data.append(values);
		}
		i++;
	}







}


void Chart::setYCols(QList<int> yCols)
{
	m_yCols = yCols;
}

/*!
  \brief In short it prepares chart. Removes all series, calls \l prepareSeries , displays legend and creates axes.
 */
void Chart::prepareChart()
{
	removeAllSeries();

        prepareSeries();
//        legend()->update();

	if (m_hasHeaders) {
		legend()->show();
	} else {
		legend()->hide();
	}

	createDefaultAxes();

//	QFont font = axisX()->labelsFont();
//	font.setPointSizeF(6);
//	axisX()->setTitleFont(font);
//	axisX()->setLabelsFont(font);



}



/*!
  \brief Renders chart into \l QBuffer containing SVG of \l QChart::size() with \l QPainter::Antialiasing
 */
QBuffer* Chart::plotToSVG()
{
	QSize s = size().toSize();

	QBuffer* b = new QBuffer;
	QSvgGenerator p;
	p.setOutputDevice(b);
//	p.setResolution(300);
	p.setSize(s); p.setViewBox(QRect(0,0,s.width(), s.height()));
	QPainter painter;
	painter.begin(&p);
	painter.setRenderHint(QPainter::Antialiasing);
	scene()->render(&painter);
	painter.end();

	return b;



}

/*!
 * \brief Renders chart into \l QImage containing PNG image of \l QChart::size() with \l QPainter::Antialiasing
 */
QImage Chart::plotToPNG()
{
	QSize s = size().toSize();

	QImage img(s, QImage::Format_RGB16);
	img.fill(Qt::white);
//	QBuffer* b = new QBuffer;
//	QSvgGenerator p;
//	p.setOutputDevice(b);
//	p.setResolution(300);
//	p.setSize(s); p.setViewBox(QRect(0,0,s.width(), s.height()));
	QPainter painter;
	painter.begin(&img);
	painter.setRenderHint(QPainter::Antialiasing);
	scene()->render(&painter);
	painter.end();

	return img;

}

/*!
  \brief Calls \l plotToSVG() and copies result into clipboard.
 */
void Chart::svgToClipboard()
{
	QBuffer* b = plotToSVG();
	QMimeData* d = new QMimeData();
	d->setData("image/svg+xml", b->buffer());
//	d->setData("text/plain", b->buffer());
	qApp->clipboard()->setMimeData(d, QClipboard::Clipboard);

	delete b;
//	delete d;

}

/*!
 * \brief Calls \l plotToPNG() and copies result into clipboard.
 */
void Chart::pngToClipboard()
{
	QImage img = plotToPNG();
	qApp->clipboard()->setImage(img, QClipboard::Clipboard);

}

/*!
 * \brief Calls \l plotToPNG() and outputs result into file specified by \a filename.
 */
void Chart::pngToFile(QString filename)
{
	QImage img = plotToPNG();
	img.save(QDir::current().absoluteFilePath(filename));
}

/*!
 * \brief Renders the chart into specified \a view. As a side effect - makes \a view resize. \l QPainter::Antialiasing is enabled for this function.
 */
void Chart::render(QGraphicsView *view)
{
    qDebug() << view->isHidden();
	view->setScene(scene());
//	QRect rect = view->geometry();
	QRect rect(0, 0, this->size().width(), this->size().height());
	QSize s(rect.width(), rect.height());
	view->setGeometry(rect);
	scene()->setSceneRect(0,0,s.width() * 0.99,s.height() * 0.99);
	view->setRenderHint(QPainter::Antialiasing);

}

/*!
 * \brief Saves this chart's settings to INI-file specified by \a filename
 */
void Chart::saveSettings(QString filename)
{
	ChartSettings s(this);
	s.save(filename);
}

/*!
 * \brief Loads this chart's settings from INI-file specified by \a filename
 */
void Chart::loadSettings(QString filename)
{
	ChartSettings s(this);
	s.load(filename);

}

/*!
 * \brief Prepares series: first cleans all the existing series, then adds one \l QSplineSeries for each column in \l yCols.
 */
void Chart::prepareSeries()
{
    removeAllSeries();
        foreach (const int& yCol, m_yCols) {
                int xCol = m_xCol;
                QSplineSeries* lSeries = new QSplineSeries();
                foreach (const QList<qreal>& row, m_data) {
                        qreal x = row.at(xCol);
                        qreal y = row.at(yCol);
                        lSeries->append(x, y);

                }
                if (m_hasHeaders) {
                        lSeries->setName(m_headers.at(yCol));
                }
                addSeries(lSeries);
        }

}

bool Chart::hasHeaders() const
{
	return m_hasHeaders;
}

QStringList Chart::headers() const
{
	return m_headers;
}

int Chart::xCol() const
{
	return m_xCol;
}


QList<int> Chart::yCols() const
{
    return m_yCols;
}


int Chart::xTicksCount() const
{
    return m_xTicksCount;
}

bool Chart::xNiceNumbers() const
{
    return m_xNiceNumbers;
}

void Chart::setHasHeaders(bool hasHeaders)
{
    if (m_hasHeaders == hasHeaders)
        return;

    m_hasHeaders = hasHeaders;
    emit hasHeadersChanged(hasHeaders);
}

void Chart::setXColumn(int xCol)
{
    if (m_xCol == xCol)
        return;

    m_xCol = xCol;
    emit xColChanged(xCol);
}

void Chart::setXTicksCount(int xTicksCount)
{
    if (m_xTicksCount == xTicksCount)
        return;

    m_xTicksCount = xTicksCount;
    emit xTicksCountChanged(xTicksCount);
}

void Chart::setXNiceNumbers(bool xNiceNumbers)
{
    if (m_xNiceNumbers == xNiceNumbers)
        return;

    m_xNiceNumbers = xNiceNumbers;
    emit xNiceNumbersChanged(xNiceNumbers);
}
