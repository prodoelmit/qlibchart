#include "csvvalue.h"


/**
 * @fn CSVValue::CSVValue
 * @param s String that comes from CSV file
 * @brief Default constructor. Tries to convert incoming string to double. If it fails, considers it a string.
 */
CSVValue::CSVValue(QString s)
{
	bool ok;
	m_number = s.toDouble(&ok);
	m_isNumber = ok;
}

bool CSVValue::isNumber() const
{
    return m_isNumber;
}

bool CSVValue::isString() const
{
    return !isNumber();
}

qreal CSVValue::number() const
{
    return m_number;
}
