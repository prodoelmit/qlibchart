#ifndef CHART_H
#define CHART_H
#include <QtCharts>
#include "qlibchart_global.h"
#include "chartsettings.h"
using namespace QtCharts;






class ChartSettings;
class QLIBCHARTSHARED_EXPORT Chart: public QChart
{
public:


    Q_PROPERTY(bool hasHeaders READ hasHeaders WRITE setHasHeaders NOTIFY hasHeadersChanged)
    Q_PROPERTY(int xCol READ xCol WRITE setXCol NOTIFY xColChanged)
    Q_PROPERTY(int xTicksCount READ xTicksCount WRITE setXTicksCount NOTIFY xTicksCountChanged)
    Q_PROPERTY(bool xNiceNumbers READ xNiceNumbers WRITE setXNiceNumbers NOTIFY xNiceNumbersChanged)
    Q_PROPERTY(QList<int> yCols READ yCols WRITE setYCols NOTIFY yColsChanged)


    Chart();
    friend class ChartSettings;
    void readCSV(QString filename, bool hasHeaders = true, QString sep = QString(","));
    void prepareChart();
    void setCSV(QString csv, QString sep = QString(","));
    void setCSV(QStringList csv, QString sep = QString(","));
    QBuffer* plotToSVG();
    QImage plotToPNG();
    void svgToClipboard();
    void pngToClipboard();
    void pngToFile(QString filename);

    void render(QGraphicsView* view);
    void saveSettings(QString filename = "/home/morodeer/charts2.ini");
    void loadSettings(QString filename);
    void prepareSeries();

    bool hasHeaders() const;
    QStringList headers() const;
    int xCol() const;
    QList<int> yCols() const;
    int xTicksCount() const;
    bool xNiceNumbers() const;

public slots:
    void setHasHeaders(bool hasHeaders);
    void setXCol(int xCol);
    void setYCols(const QList<int> &yCols);
    void setXTicksCount(int xTicksCount);
    void setXNiceNumbers(bool xNiceNumbers);
signals:
    void hasHeadersChanged(bool hasHeaders);
    void xColChanged(int xCol);
    void xTicksCountChanged(int xTicksCount);
    void yColsChanged(QList<int> yCols);
    void xNiceNumbersChanged(bool xNiceNumbers);

private:
    QSize m_size;
    QList<QList<qreal>> m_data;
    QStringList m_headers;
    int m_xCol;
    QList<int> m_yCols;
    bool m_hasHeaders;


    int xTicksCount;
    bool m_xNiceNumbers;
    int m_xTicksCount;
};

#endif // CHART_H
