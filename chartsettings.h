#ifndef CHARTSETTINGS_H
#define CHARTSETTINGS_H
#include <QString>

#include "chart.h"


class Chart;
class ChartSettings
{
public:
	ChartSettings(Chart* chart);
	void save(const QString& filename);
	void load(const QString& filename);



private:
	Chart* m_chart;


};

#endif // CHARTSETTINGS_H
